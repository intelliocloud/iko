/**
 * Created by Francois Poirier on 2018-12-03.
 */
({
    doInit : function (component, event) {

        console.log('in doInit');

        window.addEventListener("message", function (event) {
            if(event.data === true) {
                component.set("v.isPaid", event.data);
            }
        }, false);

        var accountPaymentId = component.get("v.accountPaymentId");

        console.log('accountPaymentId ===> ' + accountPaymentId);
        var action = component.get("c.getInvoicePaidStatus");

        action.setParams(
            {
                accountPaymentId: accountPaymentId
            }
        );

        action.setCallback(this, function (response) {

            var state = response.getState();
            console.log('in callback ');
            if(state === "SUCCESS"){

                var isPaid = response.getReturnValue();
                component.set("v.isPaid", isPaid);
            }
            else {
                console.log('Error: ' + JSON.stringify(response.getReturnValue()));
            }

        });

        $A.enqueueAction(action);
    }
})