/**
 * Created by Francois Poirier on 2018-12-03.
 */
({
    doInit : function (component, event, helper) {

        helper.doInit(component, event);

        //component.rerender(component);

    },
    getoken : function (component, event, helper) {

        helper.getToken(component, event);

    }
})