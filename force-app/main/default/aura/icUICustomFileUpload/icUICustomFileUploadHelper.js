// /**
//  * Created by Dongliang Zhang on 2020-02-19.
//  */
//
// /**
//  * Created by Francois Poirier on 2019-11-15.
//  */
//
// ({
//
//     doInit : function(component, event){
//
//         console.log('in doInit helper');
//
//         let uploads = component.get("v.uploads");
//         uploads.success=[];
//         component.set("v.uploads", uploads);
//
//
//         var action = component.get("c.getRebatePrograms");
//         action.setCallback(this, function (response) {
//
//             var state = response.getState();
//             if(state === "SUCCESS"){
//                 console.log('back with success');
//                 var rebatePrograms = response.getReturnValue();
//                 console.log('rebate programes ===> ' + JSON.stringify(rebatePrograms));
//                 component.set("v.rebatePrograms", rebatePrograms);
//             }
//             else {
//                 console.log('Error : ' + JSON.stringify(response));
//             }
//         });
//
//         $A.enqueueAction(action);
//     },
//
//     fileUploaded : function (component, event) {
//
//
//
//         console.log('in file uploaded');
//         var uploadedFiles = event.getParam("files");
//         var attachedFiles = component.get('v.files');
//         var numberOfFiles = attachedFiles.length;
//         var maxNumberOfFilesReached = false;
//         var maxNumberOfFiles = $A.get('$Label.c.Max_number_uploaded_invoices');
//         var alertMessage = $A.get('$Label.c.Max_number_of_uploaded_files_alert');
//
//         console.log('in fileuploaded before loop ***');
//         console.log('NumberOfFiles ===> ' + numberOfFiles);
//         console.log('maxNumberOfFiles ===> ' + maxNumberOfFiles);
//         console.log('maxNumberOfFilesReached ===> ' + maxNumberOfFilesReached);
//
//         uploadedFiles.forEach(function (file) {
//             /*if(numberOfFiles <= maxNumberOfFiles) {
//                 attachedFiles.push(file);
//                 numberOfFiles++;
//             }
//             else {
//                 maxNumberOfFilesReached = true;
//             }*/
//             if(!maxNumberOfFilesReached){
//                 attachedFiles.push(file);
//                 numberOfFiles++;
//                 if(numberOfFiles == maxNumberOfFiles){
//                     maxNumberOfFilesReached = true;
//                 }
//             }
//
//             console.log('NumberOfFiles ===> ' + numberOfFiles);
//             console.log('maxNumberOfFiles ===> ' + maxNumberOfFiles);
//             console.log('maxNumberOfFilesReached ===> ' + maxNumberOfFilesReached);
//         });
//
//         component.set("v.files", attachedFiles);
//         component.set("v.uploadCompleted", true);
//         if(maxNumberOfFilesReached){
//             console.log('before disabling upload');
//             component.set("v.disableFileUpload", true);
//             alert(alertMessage);
//             console.log('after disabling upload and alert');
//         }
//
//     },
//
//     createContractorInvoice : function (component, event) {
//
//         console.log("in createContractorInvoice");
//         var action = component.get("c.newContractorInvoice");
//         var selectedRebateProgram = component.get('v.selectedRebateProgram');
//
//         action.setParams(
//             {
//                 rebateProgram : selectedRebateProgram
//             }
//         );
//
//         action.setCallback(this, function (response) {
//
//             var state = response.getState();
//
//             if(state === "SUCCESS"){
//
//                 var invoice = response.getReturnValue();
//                 component.set("v.contractorInvoiceId", invoice.Id);
//                 console.log('invoice Id ====> ' + invoice.Id);
//                 component.set("v.contractorInvoice", invoice);
//             }
//             else {
//                 console.log('Error : ' + response);
//             }
//
//         });
//
//         $A.enqueueAction(action);
//     },
//
//     deleteFile: function (component, event) {
//
//         console.log('in deletefile med helper');
//
//         var file = event.getParam("file");
//         var fileId = file.documentId;
//         var invoiceId = component.get("v.contractorInvoiceId");
//         var action = component.get("c.deleteUploadedFile");
//         console.log('fileId ===> ' + fileId);
//         console.log('invoiceId ===> ' + invoiceId);
//
//         action.setParams(
//             {
//                 fileId: fileId,
//                 invoiceId: invoiceId
//             }
//         );
//
//         action.setCallback(this, function (response) {
//             console.log('in callback from delete files');
//
//             var state = response.getState();
//             if (state === 'SUCCESS') {
//                 console.log('in success');
//
//                 var files = response.getReturnValue();
//                 component.set("v.files", files);
//
//                 var isUploadDisabled = component.get("v.disableFileUpload");
//                 if(isUploadDisabled === true) {
//                     var numberOfFiles = files.length;
//                     var maxNumberOfFiles = $A.get('$Label.c.Max_number_uploaded_invoices');
//                     if(numberOfFiles < maxNumberOfFiles){
//                         component.set("v.disableFileUpload", false);
//                     }
//                 }
//             }
//         });
//
//         $A.enqueueAction(action);
//
//     },
//
//     saveContractorInvoice : function (component, event) {
//
//         var action = component.get("c.saveInvoice");
//         var contractorInvoice = component.get("v.contractorInvoice");
//
//         action.setParams({invoice : contractorInvoice});
//
//         $A.enqueueAction(action);
//
//     },
//
//     sendInvoice : function (component, event) {
//
//         var action = component.get("c.saveInvoice");
//         var contractorInvoice = component.get("v.contractorInvoice");
//
//         if(this.validateFields(contractorInvoice) === true) {
//             contractorInvoice.Status__c = "Ready for Upload";
//             action.setParams({invoice: contractorInvoice});
//             action.setCallback(this, function (response) {
//
//                 var state = response.getState();
//                 if (state === 'SUCCESS') {
//
//                     component.set("v.selectedRebateProgram", "");
//                     component.set("v.contractorInvoiceId", "");
//                     component.set("v.contractorInvoice", null);
//                     component.set("v.files", null);
//                     component.set("v.uploadCompleted", false);
//                     component.set("v.disableFileUpload", false);
//
//                 }
//
//             })
//             $A.enqueueAction(action);
//         }
//
//     },
//
//     validateFields : function (contractorInvoice) {
//
//         var retour = true;
//         console.log('contractor invoice.Invoice_Number__c ===> ' + contractorInvoice.Invoice_Number__c);
//
//         if(contractorInvoice.Invoice_Number__c === undefined || contractorInvoice.Invoice_Date__c === undefined ||
//             contractorInvoice.Invoice_Number__c === null || contractorInvoice.Invoice_Date__c === null ||
//             contractorInvoice.Invoice_Number__c === "" || contractorInvoice.Invoice_Date__c === ""){
//             alert("Please fill all mandatory fields before submitting");
//             retour = false;
//         }
//         return retour;
//     }
//
//
// });

/**
 * Created by Francois Poirier on 2019-11-15.
 */

({

    doInit: function (component, event) {

        console.log('in doInit helper');

        var action = component.get("c.getRebatePrograms");
        action.setCallback(this, function (response) {

            var state = response.getState();
            if (state === "SUCCESS") {
                console.log('back with success');
                var rebatePrograms = response.getReturnValue();
                console.log('rebate programes ===> ' + JSON.stringify(rebatePrograms));
                component.set("v.rebatePrograms", rebatePrograms);
            } else {
                console.log('Error : ' + JSON.stringify(response));
            }
        });

        $A.enqueueAction(action);
    },

    fileUploaded: function (component, event) {

        console.log('in file uploaded');
        var uploadedFiles = event.getParam("files");
        var attachedFiles = component.get('v.files');
        var numberOfFiles = attachedFiles.length;
        var maxNumberOfFilesReached = false;
        var maxNumberOfFiles = $A.get('$Label.c.Max_number_uploaded_invoices');
        var alertMessage = $A.get('$Label.c.Max_number_of_uploaded_files_alert');

        console.log('in fileuploaded before loop ***');
        console.log('NumberOfFiles ===> ' + numberOfFiles);
        console.log('maxNumberOfFiles ===> ' + maxNumberOfFiles);
        console.log('maxNumberOfFilesReached ===> ' + maxNumberOfFilesReached);

        uploadedFiles.forEach(function (file) {
            /*if(numberOfFiles <= maxNumberOfFiles) {
                attachedFiles.push(file);
                numberOfFiles++;
            }
            else {
                maxNumberOfFilesReached = true;
            }*/
            if (!maxNumberOfFilesReached) {
                attachedFiles.push(file);
                numberOfFiles++;
                if (numberOfFiles == maxNumberOfFiles) {
                    maxNumberOfFilesReached = true;
                }
            }

            console.log('NumberOfFiles ===> ' + numberOfFiles);
            console.log('maxNumberOfFiles ===> ' + maxNumberOfFiles);
            console.log('maxNumberOfFilesReached ===> ' + maxNumberOfFilesReached);
        });

        component.set("v.files", attachedFiles);
        component.set("v.uploadCompleted", true);
        if (maxNumberOfFilesReached) {
            console.log('before disabling upload');
            component.set("v.disableFileUpload", true);
            alert(alertMessage);
            console.log('after disabling upload and alert');
        }

    },

    createContractorInvoice: function (component, event) {

        console.log("in createContractorInvoice");
        var action = component.get("c.newContractorInvoice");
        var selectedRebateProgram = component.get('v.selectedRebateProgram');

        action.setParams(
            {
                rebateProgram: selectedRebateProgram
            }
        );

        action.setCallback(this, function (response) {

            var state = response.getState();

            if (state === "SUCCESS") {

                var invoice = response.getReturnValue();
                component.set("v.contractorInvoiceId", invoice.Id);
                console.log('invoice Id ====> ' + invoice.Id);
                component.set("v.contractorInvoice", invoice);
            } else {
                console.log('Error : ' + response);
            }

        });

        $A.enqueueAction(action);
    },

    deleteFile: function (component, event) {

        console.log('in deletefile med helper');

        var file = event.getParam("file");
        var fileId = file.documentId;
        var invoiceId = component.get("v.contractorInvoiceId");
        var action = component.get("c.deleteUploadedFile");
        console.log('fileId ===> ' + fileId);
        console.log('invoiceId ===> ' + invoiceId);

        action.setParams(
            {
                fileId: fileId,
                invoiceId: invoiceId
            }
        );

        action.setCallback(this, function (response) {
            console.log('in callback from delete files');

            var state = response.getState();
            if (state === 'SUCCESS') {
                console.log('in success');

                var files = response.getReturnValue();
                component.set("v.files", files);

                var isUploadDisabled = component.get("v.disableFileUpload");
                if (isUploadDisabled === true) {
                    var numberOfFiles = files.length;
                    var maxNumberOfFiles = $A.get('$Label.c.Max_number_uploaded_invoices');
                    if (numberOfFiles < maxNumberOfFiles) {
                        component.set("v.disableFileUpload", false);
                    }
                }
            }
        });

        $A.enqueueAction(action);

    },

    saveContractorInvoice: function (component, event) {

        var action = component.get("c.saveInvoice");
        var contractorInvoice = component.get("v.contractorInvoice");

        action.setParams({invoice: contractorInvoice});

        $A.enqueueAction(action);

    },

    sendInvoice: function (component, event) {


        var action = component.get("c.saveInvoice");
        var contractorInvoice = component.get("v.contractorInvoice");

        if (this.validateFields(contractorInvoice) === true) {
            contractorInvoice.Status__c = "Ready for Upload";
            action.setParams({invoice: contractorInvoice});
            action.setCallback(this, function (response) {

                var state = response.getState();
                if (state === 'SUCCESS') {

                    component.set("v.selectedRebateProgram", "");
                    component.set("v.contractorInvoiceId", "");
                    component.set("v.contractorInvoice", null);
                    component.set("v.files", null);
                    component.set("v.uploadCompleted", false);
                    component.set("v.disableFileUpload", false);

                }
            })
            $A.enqueueAction(action);
        }

    },

    validateFields: function (contractorInvoice) {

        var retour = true;
        console.log('contractor invoice.Invoice_Number__c ===> ' + contractorInvoice.Invoice_Number__c);

        if (contractorInvoice.Invoice_Number__c === undefined || contractorInvoice.Invoice_Date__c === undefined ||
            contractorInvoice.Invoice_Number__c === null || contractorInvoice.Invoice_Date__c === null ||
            contractorInvoice.Invoice_Number__c === "" || contractorInvoice.Invoice_Date__c === "") {
            alert("Please fill all mandatory fields before submitting");
            retour = false;
        }
        return retour;
    },


// ===============> lightning:input <==================


    onFileUploaded: function (component, event) {

        console.log('in file uploaded***');

        var uploadedFiles = event.getParam("files");
        console.log("file uploaded:===> " + uploadedFiles);

        var numberOfFiles = uploadedFiles.length;
        console.log("Number for attached files:===> " + numberOfFiles);

        var attachedFiles = component.get("v.files");

        var maxNumberOfFilesReached = false;
        var maxNumberOfFiles = $A.get('$Label.c.Max_number_uploaded_invoices');
        var alertMessage = $A.get('$Label.c.Max_number_of_uploaded_files_alert');
        console.log('maxNumberOfFiles ===> ' + maxNumberOfFiles);
        console.log('maxNumberOfFilesReached ===> ' + maxNumberOfFilesReached);

        var totalNumberFilesUploaded = attachedFiles.length;
        var sendBtnIsDisabled = component.get("v.sendBtnIsDisabled");
        console.log("sendBtnIsDisabled ============ " + sendBtnIsDisabled);

        if (!maxNumberOfFilesReached) {

            for(let i=0; i < numberOfFiles; i++){
                totalNumberFilesUploaded++;
                attachedFiles.push(uploadedFiles[i]);
                var name = attachedFiles[i].name;
                console.log("uploaded file name===> : " + name);
            }
            console.log("file attached after upload: " + attachedFiles);
            console.log("total Number Files Uploaded: ===>********* " + totalNumberFilesUploaded);
            console.log("Number of file attached after upload: " + attachedFiles.length);

            component.set("v.sendBtnIsDisabled", false);

            if (totalNumberFilesUploaded >= maxNumberOfFiles) {
                maxNumberOfFilesReached = true;
            }

        }


        component.set("v.files", attachedFiles);
        component.set("v.uploadCompleted", true);
        if(maxNumberOfFilesReached){
            console.log('before disabling upload');
            component.set("v.disableFileUpload", true);
            alert(alertMessage);
            console.log('after disabling upload and alert');
        }

        console.log("maxNumberOfFilesReached===> : " + maxNumberOfFilesReached);
        console.log("file uploaded======================>>> success");
    },

    onDeleteFile : function(component, event) {
        console.log('on handling delete file**************===========>');

        var file = event.getParam("file");
        console.log('event file===============>' + file);
    },

    onSendInvoice : function(component, event) {
        console.log('on handling send button ************************');




        var refernceNumber = component.find("referenceNumber");
        var invoiceDate = component.find("invoiceDate");
        console.log('reference number +++++++++++++++++ ' + refernceNumber );
        console.log('invoice date +++++++++++++++++ ' + invoiceDate);

        this.createContractorInvoice();

        // ***************save invoice************
        var action = component.get("c.saveInvoice");
        var contractorInvoice = component.get("v.contractorInvoice");

        console.log('saved Invoice ::::::::::::::: ' + action);

        console.log('contractor invoice +++++++++++++++++ ' + contractorInvoice);


        this.sendInvoice();

    }


});