/**
 * Created by Dongliang Zhang on 2020-02-19.
 */

/**
 * Created by Francois Poirier on 2019-11-15.
 */

({

    doInit : function(component, event, helper){

        console.log('in doInit controller');
        helper.doInit(component, event);

    },

    handleFileUploadFinish : function (component, event, helper) {

        helper.fileUploaded(component, event);

    },

    handleChangeProgram : function (component, event, helper) {

        console.log("in handleChangeProgram");
        helper.createContractorInvoice(component, event);

    },

    handleDeleteUploadedFile : function (component, event, helper) {
        console.log('in handleDeleteUploadedfile med ctrl');

        helper.deleteFile(component, event);

    },

    handleJobNumberChange : function (component, event, helper) {

        helper.saveContractorInvoice(component, event);

    },

    handleJobDateChange : function (component, event, helper) {

        helper.saveContractorInvoice(component, event);

    },

    handleSendClick : function (component, event, helper) {

        helper.sendInvoice(component, event);
    },

    handleFileSelected : function (component, event, helper) {
        let files = component.get("v.fileToBeUploaded");

        if(files && files.length>0){
            let file = files[0][0];
            let uploads = component.get("v.uploads");
            uploads["fileToBeUploadedName"] = file.name;
            component.set("v.uploads", uploads);
        }
    }
});