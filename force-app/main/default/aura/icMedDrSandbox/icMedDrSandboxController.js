({
	doInit : function(component, event, helper) {
		helper.doInit(component, event);
	},

	doNext : function(component, event, helper) {
		helper.doNext(component, event);
	},

	doPrev : function(component, event, helper) {
		helper.doPrev(component, event);
	},

	adjustDisplayedCards : function(component, event, helper) {
		helper.adjustDisplayedCards(component, event);
	}
})