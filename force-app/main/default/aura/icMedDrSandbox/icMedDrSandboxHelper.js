({
	doInit : function(component, event) {
		var action = component.get("c.getWhatsHappening");

		action.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				var responseValue = response.getReturnValue();
				component.set("v.listWhatsHappening", responseValue);
				
				var displayWhatsHappening = [];
				
				var listCap = 3;
				if(listCap > responseValue.length) {
					listCap = responseValue.length
				}
				
				for(var i = 0; i < listCap; i++) {
					displayWhatsHappening.push(responseValue[i]);
				}
				
				component.set("v.displayWhatsHappening", displayWhatsHappening);
			}
		});

		$A.enqueueAction(action);
	},

	doNext : function(component, event) {
		var carouselPage = component.get("v.carouselPage");
		carouselPage++;
		component.set("v.carouselPage", carouselPage);
		this.adjustDisplayedCards(component, event, carouselPage);
	},

	doPrev : function(component, event) {
		var carouselPage = component.get("v.carouselPage");
		carouselPage--;
		component.set("v.carouselPage", carouselPage);
		this.adjustDisplayedCards(component, event, carouselPage);
	},

	adjustDisplayedCards : function(component, event, carouselPage) {
		var listWhatsHappening = component.get("v.listWhatsHappening");		
		var displayWhatsHappening = [];

		if(carouselPage < 0){
			carouselPage = Math.floor(listWhatsHappening.length/3);
			component.set("v.carouselPage", carouselPage);
		}

		var listIndex = carouselPage*3;
		if(listIndex > listWhatsHappening.length) {
			listIndex = 0;
			component.set("v.carouselPage", 0);
		}

		var listCap = listIndex+3;
		if(listCap > listWhatsHappening.length) {
			listCap = listWhatsHappening.length
		}
		
		for(var i = listIndex; i < listCap; i++) {
			displayWhatsHappening.push(listWhatsHappening[i]);
		}

		component.set("v.displayWhatsHappening", displayWhatsHappening);
	}

})