/**
 * Created by Francois Poirier on 2019-12-18.
 */

({
    doInit : function (component, event, helper) {

        helper.doInit(component, event);

    },

    handleCloseClick : function (component, event, helper) {

        helper.closeModal(component, event);

    },

    handleSelectedValueChange : function (component, event, helper) {

        console.log('selectedValue Change ****');
        helper.showMessage(component, 'Choices');
    }

});