({
	onFileUploaded : function (component, event) {

		var uploadedFiles = event.getParam("files");
		var numberOfFiles = uploadedFiles.length;

		var attachedFiles = component.get("v.files");

		var file = uploadedFiles[0];

		var sessionId = component.get("v.sessionId");
		var pathToDetail = component.get("v.pathToDetail");
		var sfInstanceUrl = 'https://iko--l3.lightning.force.com/';
		//var spinnerSubmit = component.find("spinnerSubmit");
		var client = new forcetk.Client();
		client.setSessionToken(sessionId ,'v36.0',sfInstanceUrl);
		client.proxyUrl = null;
		client.instanceUrl = sfInstanceUrl;
		console.log('----45----',file);

		var self = this;

		client.createBlob('ContentVersion', {
				'ParentId': 'a0M1F000002pq63UAA',
				'Name': file.name,
				'ContentType': file.type,
			},

			file.name, 'Body', file , function(response)
			{
				if(response.id !== null)
				{
					//$A.util.addClass(spinnerSubmit,"hide");
					//self.goToURL(pathToDetail + resId);
				}
				else
				{
					//$A.util.addClass(spinnerSubmit,"hide");
					component.set("v.strAttachmentError",'Error : '+response.errors[0].message);
				}
			},
			function(request, status, response)
			{
				console.log('----46----',request);
				//$A.util.addClass(spinnerSubmit,"hide");
				var res = JSON.parse(response);
				component.set("v.strAttachmentError",'Error : ' +res[0].message);

			});

	},
})