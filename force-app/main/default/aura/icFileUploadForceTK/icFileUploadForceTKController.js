({
	onForceTKInit : function(component, event, helper) {
		
		var action=component.get("c.retrieveSessionID");

		action.setCallback(this, function (response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				var data= response.getReturnValue();
				component.set("v.sessionId",data);


			} else if (state === "ERROR") {

			}
		});

		$A.enqueueAction(action);

	},

	onFileUploaded : function(component, event, helper){
		helper.onFileUploaded(component, event);
	},
})