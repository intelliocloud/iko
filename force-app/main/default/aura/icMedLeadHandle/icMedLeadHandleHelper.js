({
	doReject : function (component, event, helper) {

		var recordId = component.get("v.recordId");

		var action = component.get("c.handleLeadRejection");
		action.setParams({leadId : recordId});
		action.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				var responseValue = response.getReturnValue();

					$A.get("e.force:closeQuickAction").fire();
					var toastEvent = $A.get("e.force:showToast");
					toastEvent.setParams({
						"type": "warning",
						"message": 'This Lead was Rejected'
					});
					toastEvent.fire();
				$A.get('e.force:refreshView').fire();
			}
			else {
				console.log('erreur : ' + JSON.stringify(response));
			}
		});

		$A.enqueueAction(action);


	},
	doConvert : function (component, event, helper) {

		var recordId = component.get("v.recordId");

		var action = component.get("c.handleLeadConversion");
		action.setParams({leadId : recordId});
		action.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				var responseValue = response.getReturnValue();

				if (responseValue.includes(' ')) {
					$A.get("e.force:closeQuickAction").fire();
					var toastEvent = $A.get("e.force:showToast");
					toastEvent.setParams({
						"type": "error",
						"message": responseValue
					});
					toastEvent.fire();
				} else {

					$A.get("e.force:closeQuickAction").fire();
					var toastEvent = $A.get("e.force:showToast");
					toastEvent.setParams({
						"type": "success",
						"message": 'Lead Converted Successfully!'
					});
					toastEvent.fire();

					var navEvt = $A.get("e.force:navigateToSObject");
					navEvt.setParam("recordId", responseValue);
					navEvt.fire();
				}
			}
			else {
				console.log('erreur : ' + JSON.stringify(response));
			}
		});

		$A.enqueueAction(action);
	}
})