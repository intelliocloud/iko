/**
 * Created by Dongliang Zhang on 2020-04-23.
 */
({
    formatValue : function (component, event) {

        var value = component.get("v.value");
        console.log("value before format ====> " + value);
        if(value) {
            var formattedValue = this.numberWithCommas(value);
            component.set("v.formattedValue", formattedValue);
            console.log("value after format *****> " + formattedValue);
        }else{
            component.set("v.formattedValue", "0");
        }
    },

    numberWithCommas: function (value) {
        // var getValueDec = Math.abs(value) - Math.floor(value);
        var returnValue = value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        //
        // if (getValueDec <= 0.5 && getValueDec > 0){
        //     returnValue = Math.floor(value).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + '.50';
        // }
        //
        // if(getValueDec > 0.5 && getValueDec < 1){
        //     returnValue = Math.round(value).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        // }

        return returnValue;
    }
});