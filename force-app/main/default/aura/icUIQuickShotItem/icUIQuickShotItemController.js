/**
 * Created by Francois Poirier on 2019-10-24.
 */

({
    formatValue : function (component, event, helper) {
        helper.formatValue(component, event);
    }
});