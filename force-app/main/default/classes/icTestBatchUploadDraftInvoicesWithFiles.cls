/**
 * Created by Francois Poirier on 2020-01-21.
 */

@isTest
public with sharing class icTestBatchUploadDraftInvoicesWithFiles {

    @isTest
    public static void testBatch() {

        icTestMockUtilities.Mocker.SetMockSingletonInstance('icLogicInvoice', new icLogicInvoiceMock());

        Contractor_Invoice__c invoice = new Contractor_Invoice__c();
        invoice.Status__c = 'Draft';
        invoice.File_s_Attached__c = true;
        insert invoice;

        Database.executeBatch(new icBatchUploadDraftInvoicesWithFiles(), 100);

    }
}