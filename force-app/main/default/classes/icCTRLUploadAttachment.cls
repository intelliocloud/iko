public class icCTRLUploadAttachment {
    
    public Blob attBody { get; set; }
    public String fileName { get; set; }
    public Integer fileSize { get; set; }
    public String contentType { get; set; }
    private Id parentRecordId;

    private icLogicAttachment.IClass logicAttachment = (icLogicAttachment.IClass) icObjectFactory.GetSingletonInstance('icLogicAttachment');
    
    public icCTRLUploadAttachment(){
        
        parentRecordId = ApexPages.currentPage().getParameters().get('id');
        
    }
    
    public PageReference uploadFile() {
        
        if( attBody != null ) {
            try { 
                logicAttachment.createDocumentInObject(parentRecordId, fileName, fileName, attBody);
                ApexPages.addMessage(new ApexPages.Message( ApexPages.severity.CONFIRM, Label.icFile_uploaded_successfully) );
            } catch ( Exception e ) {
                ApexPages.addMessage(new ApexPages.Message( ApexPages.severity.ERROR, Label.icFile_upload_failed_due_to + '"' + e.getMessage() + '".') );
            }
            
            attBody = null;
            fileName = '';
        }
        else {
            ApexPages.addMessage(new ApexPages.Message( ApexPages.severity.ERROR, Label.icPlease_choose_a_file) );
        }
        
        PageReference pg = Page.icPageUploadAttachment;
        pg.setRedirect( false );
        return pg;
    }
}