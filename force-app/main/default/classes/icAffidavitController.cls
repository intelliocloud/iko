public with sharing class icAffidavitController {
    private final static String INSTALLATION_FUNDAMENTALS        = Schema.getGlobalDescribe().get('Account').getDescribe().fields.getMap().get('IF_Course_Affirmation__c').getDescribe().getLabel().toLowerCase();
    private final static String SAFETY_FUNDAMENTALS              = Schema.getGlobalDescribe().get('Account').getDescribe().fields.getMap().get('Safety_Fundamentals_Completed__c').getDescribe().getLabel().toLowerCase();
    private final static String INSTALLATION_FUNDAMENTALS_FRENCH = 'LES BASES DE L’INSTALLATION'.toLowerCase();
    private final static String SAFETY_FUNDAMENTALS_FRENCH       = 'LES BASES DE LA SÉCURITÉ'.toLowerCase();
    @AuraEnabled
    public static void submitAffidavit(String LAID) {
        User user = [
                SELECT AccountId
                FROM User
                WHERE Id = :UserInfo.getUserId()
        ];
        Account account = [
                SELECT Name, IF_Course_Affirmation__c, Safety_Fundamentals_Completed__c
                FROM Account
                WHERE Id = :user.AccountId
        ];
        String learningPlanName = getContractorInfo(LAID).learningPlanName;
        if (isItInstallationFundamentals(learningPlanName)) {
            account.IF_Course_Affirmation__c = true;
        } else if (isItSafetyFundamentals(learningPlanName)) {
            account.Safety_Fundamentals_Completed__c = true;
        }
        update account;

        markLearningAsCompleted(LAID);
    }
    @AuraEnabled
    public static Contractor getContractorInfo(String LAID) {
        redwing__Learning_Assignment__c reLearningAssignment = [
                SELECT redwing__Community_Id__c, redwing__Training_Plan__r.Name
                FROM redwing__Learning_Assignment__c
                WHERE Id = :LAID
        ];
        User user = [
                SELECT AccountId
                FROM User
                WHERE Id = :UserInfo.getUserId()
        ];
        Account account = [
                SELECT Name, Contractor_ID__c, Safety_Fundamentals_Completed__c, IF_Course_Affirmation__c
                FROM Account
                WHERE Id = :user.AccountId
        ];
        Boolean isCompletedPlan = false;
        if (
            (
                isItInstallationFundamentals(reLearningAssignment.redwing__Training_Plan__r.Name.toLowerCase())
                && 
                account.IF_Course_Affirmation__c == true
            )
            || 
            (
                isItSafetyFundamentals(reLearningAssignment.redwing__Training_Plan__r.Name.toLowerCase()) 
                && 
                account.Safety_Fundamentals_Completed__c == true
            )
        ) {
            isCompletedPlan = true;
            markLearningAsCompleted(LAID);
        }
        return new Contractor(account.Name, account.Contractor_ID__c, reLearningAssignment.redwing__Training_Plan__r.Name, isCompletedPlan);
    }

    private static void markLearningAsCompleted(String laid) {
        
        redwing__Learning_Assignment__c affidavitLearningAssignment = [
                SELECT redwing__Progress__c, redwing__Progress_Percentage__c, redwing__Evaluation_Result__c, redwing__Completed_On__c
                FROM redwing__Learning_Assignment__c
                WHERE Id = :laid
        ];

        if (affidavitLearningAssignment.redwing__Progress__c == 'Completed') return;
        
        affidavitLearningAssignment.redwing__Progress__c = 'Completed';
        affidavitLearningAssignment.redwing__Progress_Percentage__c = 100;
        affidavitLearningAssignment.redwing__Evaluation_Result__c = 'N/A';

        update affidavitLearningAssignment;
    }

    private static Boolean isItInstallationFundamentals(String planName) {
        return INSTALLATION_FUNDAMENTALS.contains(planName.toLowerCase()) || INSTALLATION_FUNDAMENTALS_FRENCH == planName.toLowerCase();
    }

    private static Boolean isItSafetyFundamentals(String planName) {
        return SAFETY_FUNDAMENTALS.contains(planName.toLowerCase()) || SAFETY_FUNDAMENTALS_FRENCH == planName.toLowerCase();
    }

    public class Contractor {
        @AuraEnabled
        public String companyName { get; set; }
        @AuraEnabled
        public String contractorId { get; set; }
        @AuraEnabled
        public String learningPlanName { get; set; }
        @AuraEnabled
        public Boolean isCompletedPlan { get; set; }

        public Contractor(String companyName, String contractorId, String learningPlanName, Boolean isCompletedPlan) {
            this.companyName = companyName;
            this.contractorId = contractorId;
            this.learningPlanName = learningPlanName;
            this.isCompletedPlan = isCompletedPlan;
        }
    }
}