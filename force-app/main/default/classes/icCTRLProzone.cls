global with sharing class icCTRLProzone {

	static icLogicUser.IClass logicUser = (icLogicUser.IClass) icObjectFactory.GetSingletonInstance('icLogicUser');

	@AuraEnabled
	global static icDTOProzoneRemoteAuth getProzoneRemoteAuthDetails(String recordId) {
		icDTOProzoneRemoteAuth dtoRemoteAuth = new icDTOProzoneRemoteAuth();

		if(String.isBlank(recordId)) {
			dtoRemoteAuth = logicUser.getCommunityUserInfoById(UserInfo.getUserId());
		}
		system.debug('user Id in prozone ===> ' + UserInfo.getUserId());
		String sId = UserInfo.getSessionId();
		system.debug('session Id in prozone ===> ' + sId);
		dtoRemoteAuth.sessionId = UserInfo.getSessionId();		
		return dtoRemoteAuth;
	}

	@AuraEnabled
	global static String getCurrentTheme() {
		return UserInfo.getUiThemeDisplayed();
	}
}