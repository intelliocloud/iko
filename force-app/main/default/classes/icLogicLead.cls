public class icLogicLead implements icIClass {

	public Object GetInstance() {
		return new Impl();
	}

	public Interface IClass {
		Integer getNewLeadsCountByAccountId(String recordId, String ownerId);
        Lead getLeadInfoById(Id leadId);
		Boolean leadHasAddressPopulated(Lead l);
		Lead updateLead(Lead leadToUpdate);
	}

	public class Impl implements IClass {

		icRepoLead.IClass repository = (icRepoLead.IClass) icObjectFactory.GetSingletonInstance('icRepoLead');

		public Integer getNewLeadsCountByAccountId(String recordId, String ownerId) {
			List<Lead> listLeads = repository.getNewLeadsByAccountId(recordId, ownerId);
			return listLeads.size();
		}
        
        public Lead getLeadInfoById(Id leadId) {

			Lead leadInfo = repository.getLeadInfoById(leadId);

			if(leadInfo != null){
				return leadInfo;
			}
			return null;

		}

		public Boolean leadHasAddressPopulated(Lead l) {

			if(l.City != null && l.State != null && l.Street != null && l.PostalCode != null && l.CountryCode != null){
				return true;
			}
			return false;


		}

		public Lead updateLead(Lead leadToUpdate) {
			return repository.updateLead(leadToUpdate);
		}
	}
}