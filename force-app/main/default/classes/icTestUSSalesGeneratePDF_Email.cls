/**
 * Created by Anil Mannem (Incloud) on 25-Aug-2020
 */
@isTest
public with sharing class icTestUSSalesGeneratePDF_Email {
    static testMethod void test_generatePDF(){
        Test.startTest();
        Id buildingLocationRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Building_Location').getRecordTypeId();
        Id distributorRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('US_Distributor').getRecordTypeId();
        Id contractorRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('US_Contractor').getRecordTypeId();
        
        Account testBuildingLocation = new Account();
        testBuildingLocation.Name = ' Test Building Location';
        testBuildingLocation.RecordTypeId = buildingLocationRecordTypeId;
        insert testBuildingLocation;
        Opportunity testOpportunity = icTestHelperDataCreator.generateOpportunityByRecordType('US Sales Bid', testBuildingLocation.Id);
        testOpportunity.Quote_Approved__c = true;
        insert testOpportunity;
        
        Opportunity testOpportunity1 = icTestHelperDataCreator.generateOpportunityByRecordType('US Sales Bid', testBuildingLocation.Id);
        insert testOpportunity1;
        
        Account testDistributor = new Account();
        testDistributor.Name = 'Test Distributor';
        testDistributor.RecordTypeId = distributorRecordTypeId;
        insert testDistributor;

        Account testContractor = new Account();
        testContractor.Name = 'Test Contrator';
        testContractor.RecordTypeId = contractorRecordTypeId;
        insert testContractor;

        String recordId = '';
        
        // With Contractor and Quote Approved
        Bidder__c newBidder = new Bidder__c(Opportunity__c = testOpportunity.Id, Distributor_Account__c = testDistributor.Id, Contractor_Account__c = testContractor.Id);
        insert newBidder;
        recordId = newBidder.Id;
        icUSSalesGeneratePDF_Email.generatePDFandEmail(recordId);

        // Without Contractor and Quote Approved
        Bidder__c newBidder1 = new Bidder__c(Opportunity__c = testOpportunity.Id, Distributor_Account__c = testDistributor.Id);
        insert newBidder1;
        recordId = newBidder1.Id;
        icUSSalesGeneratePDF_Email.generatePDFandEmail(recordId);

        // Without Contractor and Quote not Approved
        Bidder__c newBidder2 = new Bidder__c(Opportunity__c = testOpportunity1.Id, Distributor_Account__c = testDistributor.Id);
        insert newBidder2;
        recordId = newBidder2.Id;
        icUSSalesGeneratePDF_Email.generatePDFandEmail(recordId);
        Test.stopTest();
    }
}