public class GreenSky_TriggerHandler {

    // SF-184 Set Currency of Greensky Application Automatically
    // https://ikoussfsc.atlassian.net/browse/SF-184
    public static void setCurrencyIsoCode(List<GreenSky_Application__c> applications) {
		Set<Id> contractorIds = new Set<Id>();
        for(GreenSky_Application__c a : applications) {
            contractorIds.add(a.Account__c);
        }
        
        // Build a map of Conractors so we can get the Currency Code using the ID
        List<Account> allContractors = new List<Account>([SELECT Id, CurrencyIsoCode FROM Account WHERE Id IN :contractorIds]);
        Map<Id, Account> contractorsById = new Map<Id, Account>();
        for(Account a : allContractors) { contractorsById.put(a.Id, a); }
        
        // Set the GreenSky application's currency code
        for(GreenSky_Application__c a : applications) {
            a.CurrencyIsoCode = contractorsById.get(a.Account__c).CurrencyIsoCode;
        }
    }
}