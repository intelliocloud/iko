/**
 * Created by Francois Poirier on 2019-11-01.
 */
@isTest
public with sharing class icTestDTOMembershipRenewal {

    @isTest
    static void testDtoMembershipRenewal(){

        icDTOMembershipRenewal dto = new icDTOMembershipRenewal();
        dto.remoteAuth = new icDTOProzoneRemoteAuth();
        dto.remoteAuth.accountId = icTestHelperUtility.getFakeId(Account.SObjectType);
        dto.remoteAuth.sessionId = icTestHelperUtility.generateRandomString(10);
        dto.remoteAuth.userLanguage = icTestHelperUtility.generateRandomString(10);
        dto.accountPaymentId = icTestHelperUtility.getFakeId(IKOPRO_Account_Payment__c.SObjectType);

    }
}