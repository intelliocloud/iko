/**
 * Created by Andrea Pissinis on 2018-09-13.
 */
@isTest
public with sharing class icTestServiceEagleViewFileDelivery {

	public static testMethod void test_doPost(){
		icTestMockUtilities.Mocker.SetMockSingletonInstance('icLogicAttachment', new icLogicAttachmentMock());
		Test.setMock(HttpCalloutMock.class, new icHTTPCalloutServiceMock());

		RestContext.request = new RestRequest();
		RestContext.request.requestBody = Blob.valueOf('Some Text');
		RestContext.request.addParameter('RefId', icTestHelperUtility.getFakeId(EagleView_Report__c.SObjectType));
		RestContext.request.addParameter('ReportId','ReportId');
		RestContext.request.addParameter('FileFormatId','FileFormatId');
		RestContext.request.addParameter('FileTypeId','FileTypeId');
		
		icServiceEagleViewFileDelivery.doPost();
	}

	public static testMethod void test_doPost_Exception(){
		icTestMockUtilities.Mocker.SetMockSingletonInstance('icLogicAttachment', new icLogicAttachmentMock());
		Test.setMock(HttpCalloutMock.class, new icHTTPCalloutServiceMock());

		RestContext.request = new RestRequest();
		RestContext.request.requestBody = EncodingUtil.base64Decode('Some Text');
		RestContext.request.addParameter('RefId','RefId');
		RestContext.request.addParameter('ReportId','ReportId');
		RestContext.request.addParameter('FileFormatId','FileFormatId');
		RestContext.request.addParameter('FileTypeId','FileTypeId');
		
		icServiceEagleViewFileDelivery.doPost();
	}

}