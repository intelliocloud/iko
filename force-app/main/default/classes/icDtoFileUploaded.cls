/**
 * Created by Dongliang Zhang on 2020-03-04.
 */

global with sharing class icDtoFileUploaded {
 
    @AuraEnabled global String name{get;set;}
    @AuraEnabled global String size{get;set;}
    @AuraEnabled global String type{get;set;}
    @AuraEnabled global Boolean isSaved{get;set;}
}