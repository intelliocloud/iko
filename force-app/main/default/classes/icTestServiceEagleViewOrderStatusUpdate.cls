/**
 * Created by Andrea Pissinis on 2018-09-13.
 */
@isTest
public with sharing class icTestServiceEagleViewOrderStatusUpdate {

	public static testMethod void test_doGet(){
		icTestMockUtilities.Mocker.SetMockSingletonInstance('icLogicEagleViewReport', new icLogicEagleViewReportMock());

		RestContext.request = new RestRequest();
		RestContext.request.requestBody = Blob.valueOf('<<<<<');
		RestContext.request.addParameter('StatusId','1');
		RestContext.request.addParameter('SubStatusId','1');
		RestContext.request.addParameter('RefId', icTestHelperUtility.getFakeId(EagleView_Report__c.SObjectType));
		RestContext.request.addParameter('ReportId','ReportId');

		icServiceEagleViewOrderStatusUpdate.doGet();
	}

	public static testMethod void test_doGet_exception(){
		icTestMockUtilities.Mocker.SetMockSingletonInstance('icLogicEagleViewReport', new icLogicEagleViewReportMock());

		RestContext.request = new RestRequest();
		RestContext.request.requestBody = Blob.valueOf('<<<<<');
		RestContext.request.addParameter('StatusId','1');
		RestContext.request.addParameter('SubStatusId','1');
		RestContext.request.addParameter('RefId', 'RefId');
		RestContext.request.addParameter('ReportId','ReportId');

		icServiceEagleViewOrderStatusUpdate.doGet();
	}

}