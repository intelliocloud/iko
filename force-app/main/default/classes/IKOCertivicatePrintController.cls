public without sharing class IKOCertivicatePrintController {
    public redwing__Achievement__c achievement {get; private set;}
	public redwing__Achievement_Assignment__c achievementAssignment {get; private set;}
	public DateTime acquiredDate {get; private set;}

	public PageReference init() {
		Id achievementId = ApexPages.currentPage().getParameters().get('aid');
		Id achievementAssignmentId = ApexPages.currentPage().getParameters().get('aaid');

		User u = [SELECT Id, ContactId, IsActive
							FROM User
							WHERE Id = :UserInfo.getUserId() AND IsActive = true
							LIMIT 1
		];

		List<redwing__Achievement_Assignment__c> achievementAssignments = [
			SELECT Id, CreatedDate, 
            			LastModifiedDate, 
            			redwing__Status__c, 
            			redwing__User__r.Name, 
            			redwing__Achievement__r.Name, 
            			redwing__Achievement__r.redwing__Type__c, 
            			redwing__Training_Plan_Achievement__r.redwing__Training_Plan__r.Name
			FROM redwing__Achievement_Assignment__c
			WHERE Id = :achievementAssignmentId
			AND redwing__Achievement__r.redwing__Enable_Printing__c = true
			AND (
				redwing__User__c = :u.Id
				OR (
					redwing__User__c = null
					AND redwing__Contact__c != null
					AND redwing__Contact__c = :u.ContactId
				)
			)
			AND redwing__Status__c = 'Acquired'
		];
		if (achievementAssignments != null && achievementAssignments.size() > 0) {
			achievementAssignment = achievementAssignments[0];
			achievement = achievementAssignment.redwing__Achievement__r;
			acquiredDate = achievementAssignment.LastModifiedDate;
		}

		return null;
	}


}