global with sharing class icCtrlHandleLeadChoice {

    static icLogicLead.IClass leadLogic = (icLogicLead.IClass) icObjectFactory.GetSingletonInstance('icLogicLead');

    @AuraEnabled
    global static void handleLeadRejection(Id leadId){

        Lead leadToReject = leadLogic.getLeadInfoById(leadId);

        if(leadToReject != null && leadToReject.Status != 'Rejected' && !leadToReject.IsConverted){

            leadToReject.Status = 'Rejected';
            leadLogic.updateLead(leadToReject);

        }

    }

    @AuraEnabled
    global static String handleLeadConversion(Id leadId){

        Lead leadToConvert = leadLogic.getLeadInfoById(leadId);

        if(leadToConvert != null && leadLogic.leadHasAddressPopulated(leadToConvert) && leadToConvert.Status != 'Rejected'){

            Database.LeadConvert lc = new Database.LeadConvert();
            lc.setLeadId(leadToConvert.id);

            LeadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1];
            lc.setConvertedStatus(convertStatus.MasterLabel);

            lc.setOpportunityName(leadToConvert.Name+' Opportunity');
           //lc.setAccountId(leadToConvert.PartnerAccountId); can be used IF we know we have an Account on the Lead

            Database.LeadConvertResult lcr = Database.convertLead(lc);
            System.assert(lcr.isSuccess());
            return lcr.getOpportunityId();

        }else if (leadToConvert != null && !leadLogic.leadHasAddressPopulated(leadToConvert) && leadToConvert.Status != 'Rejected'){

            return 'You need to have a full Address to Convert the Lead';

        }else if(leadToConvert.Status == 'Rejected'){
            return 'You can\'t Convert a Rejected Lead.';
        }

        return null;

    }

}