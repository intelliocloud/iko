/**
 * Created by Francois Poirier on 2018-12-17.
 */

@isTest(SeeAllData=true)
public with sharing class icTestLogicContact {

    static void setup(){

        icTestMockUtilities.Mocker.SetMockSingletonInstance('icRepoContact', new icRepoContactMock() );
        icTestMockUtilities.Mocker.SetMockSingletonInstance('icLogicEmail', new icLogicEmailMock() );
        icTestMockUtilities.Mocker.SetMockSingletonInstance('icLogicConfig',new icLogicConfigMock());

        icTestMockUtilities.Tracer.SetReturnValue('icLogicConfigMock', 'getIkoGlobalConfigByName', new Iko_Global_Configurations__mdt());

        //icTestMockUtilities.Tracer.SetReturnValue('icLogicEmailMock', 'getOrgWideEmailByName', new OrgWideEmailAddress());
        icTestMockUtilities.Tracer.SetReturnValue('icLogicEmailMock', 'getEmailTemplateByDevName', new EmailTemplate(Subject='Subject', Body='Body'));

        icTestMockUtilities.Tracer.SetReturnValue('icRepoContactMock', 'getContactById', new Contact());
        icTestMockUtilities.Tracer.SetReturnValue('icRepoContactMock', 'getPrimaryAdminContactsByContractorIds', new List<Contact>());
        icTestMockUtilities.Tracer.SetReturnValue('icRepoContactMock', 'saveContact', new Contact());
    }

    @isTest static void testGetContactById(){

        setup();

        icLogicContact.IClass contactLogic = (icLogicContact.IClass) icObjectFactory.GetSingletonInstance('icLogicContact');

        Contact testContact = contactLogic.getContactById(icTestHelperUtility.getFakeId(Contact.SObjectType));

    }

    @isTest static void test_getPrimaryAdminContactsByContractorIds(){

        setup();

        icLogicContact.IClass contactLogic = (icLogicContact.IClass) icObjectFactory.GetSingletonInstance('icLogicContact');

        List<String> contractorIds = new List<String>();

        List<Contact> testResult = contactLogic.getPrimaryAdminContactsByContractorIds(contractorIds);

    }

    @isTest static void test_getMapPrimaryAdminContactByContractorId(){

        setup();

        icLogicContact.IClass contactLogic = (icLogicContact.IClass) icObjectFactory.GetSingletonInstance('icLogicContact');

        List<Contact> contacts = new List<Contact>();
        Contact newContact = new Contact(AccountId = icTestHelperUtility.getFakeId(Account.SObjectType));
        contacts.add(newContact);

        Map<String, Contact> testContact = contactLogic.getMapPrimaryAdminContactByContractorId(contacts);

    }

    @isTest static void testSaveContact(){

        setup();

        icLogicContact.IClass contactLogic = (icLogicContact.IClass) icObjectFactory.GetSingletonInstance('icLogicContact');

        Contact testContact = new Contact();
        testContact.FirstName = icTestHelperUtility.generateRandomString(10);
        testContact.LastName = icTestHelperUtility.generateRandomString(15);
        Account testAccount = icTestHelperUtility.generateAccount();
        insert testAccount;
        testContact.AccountId = testAccount.Id;

        testContact = contactLogic.saveContact(testContact);

    }

    @isTest static void test_handleVendorEmail_US_Applicant(){

        setup();

        icTestMockUtilities.Tracer.SetReturnValue('icRepoContactMock', 'getContactsForVendorEmailById', new Contact(Country_Code_Applicant__c = 'US'));

        icLogicContact.IClass contactLogic = (icLogicContact.IClass) icObjectFactory.GetSingletonInstance('icLogicContact');

        contactLogic.handleVendorEmail('contactId');

    }

    @isTest static void test_handleVendorEmail_CA_Applicant(){

        setup();

        icTestMockUtilities.Tracer.SetReturnValue('icRepoContactMock', 'getContactsForVendorEmailById', new Contact(Country_Code_Applicant__c = 'CA'));

        icLogicContact.IClass contactLogic = (icLogicContact.IClass) icObjectFactory.GetSingletonInstance('icLogicContact');

        contactLogic.handleVendorEmail('contactId');

    }
}