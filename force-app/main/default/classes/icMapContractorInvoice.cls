/**
 * Created by Dongliang Zhang on 2020-03-05.
 */

public with sharing class icMapContractorInvoice implements icIclass {
    public Object GetInstance(){
        return new Impl();
    }

    public interface IClass{
        icDtoInvoiceCreation mapToDto(Contractor_Invoice__c contractorInvoice);
        Contractor_Invoice__c mapToContractorInvoice(icDtoInvoiceCreation dtoInvoiceCreation);
    }

    public class Impl implements IClass{

        public icDtoInvoiceCreation mapToDto(Contractor_Invoice__c contractorInvoice){
            system.debug('Contractor_Invoice__c : ' + contractorInvoice);

            icDtoInvoiceCreation dtoInvoice = new icDtoInvoiceCreation();



            return dtoInvoice;
        }

        public Contractor_Invoice__c mapToContractorInvoice(icDtoInvoiceCreation dtoInvoiceCreation) {
            system.debug('icDtoInvoiceCreation : ' + dtoInvoiceCreation);

            Contractor_Invoice__c contractorInvoice = new Contractor_Invoice__c();


            return contractorInvoice;
        }
    }

}