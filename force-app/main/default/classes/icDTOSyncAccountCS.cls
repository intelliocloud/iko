global with sharing class icDTOSyncAccountCS {
	global String contractorId {get;set;}		//Contractor_ID__c
	global String contractorName {get;set;}	//Name
	global String doingBusinessAs {get;set;}	//DBA_Account__c
	global String website {get;set;}	//Website
	global String queueId {get;set;}	//IKOPRO_QueueID__c   required
	global String phone {get;set;}	//Phone
	global String fax {get;set;}	//Fax
	global String street {get;set;}	//BillingStreet
	global String city {get;set;}	//BillingCity
	global String state {get;set;}	//BillingState
	global String postalCode {get;set;}	//BillingPostalCode
	global String country {get;set;}	//BillingCountry
	global Boolean wantsToBeListed {get;set;}	//Account>>ApplicantInfo>>Wants To Be Listed On IKO.com >> WE SHOULD CREATE A FORMULA FILED TO FETCH THIS AT THE ACCOUNT LEVEL?
	global String currentTier {get;set;}	//Present_Tier_Lookup__c >> THIS VALUE WILL BE A SF ID; SHOULD WE FETCH A FIELD FROM THE RELATED RECORD OR USE ANOTHER FIELD ON ACCOUNT?
	global String languagePreference {get;set;}	//Language_Preferences__c >> SHOULD THIS BE Business_Operating_Language_ROOFPRO__c? (multi picklist vs picklist)
}