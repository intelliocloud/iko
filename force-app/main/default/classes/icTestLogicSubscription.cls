/**
 * Created by Andrea Pissinis on 2018-09-13.
 */
@isTest
public with sharing class icTestLogicSubscription {

    static void initTest() {
        icTestMockUtilities.Mocker.SetMockSingletonInstance('icRepoSubscription', new icREPOSubscriptionMOck());
        icTestMockUtilities.Tracer.SetReturnValue('icREPOSubscriptionMOck', 'getSubscriptionsByAccountIdAndRecordTypeId', new List<IKOPRO_Contractor_Subscription__c>{new IKOPRO_Contractor_Subscription__c()});
        icTestMockUtilities.Tracer.SetReturnValue('icREPOSubscriptionMOck', 'createSubscription', new Account());
        icTestMockUtilities.Tracer.SetReturnValue('icREPOSubscriptionMOck', 'updateSubscription',new Account());
    }

    public static testMethod void test_getEagleViewSubscriptionForCustomer(){
        initTest();
        icLogicSubscription.IClass BL = (icLogicSubscription.IClass) icObjectFactory.GetSingletonInstance('icLogicSubscription');        

        BL.getEagleViewSubscriptionForCustomer('');
    }

    public static testMethod void test_getGreenSkySubscriptionForCustomer(){
        initTest();
        icLogicSubscription.IClass BL = (icLogicSubscription.IClass) icObjectFactory.GetSingletonInstance('icLogicSubscription');        

        BL.getGreenSkySubscriptionForCustomer('');
    }

    public static testMethod void test_createEagleViewSubscription(){
        icTestMockUtilities.Mocker.SetMockSingletonInstance('icRepoSubscription', new icREPOSubscriptionMOck());

        icLogicSubscription.IClass BL = (icLogicSubscription.IClass) icObjectFactory.GetSingletonInstance('icLogicSubscription');


        BL.createEagleViewSubscription(icTestHelperUtility.getFakeId(Account.SObjectType),'');

    }

    public static testMethod void test_createGreenSkySubscription(){
        icTestMockUtilities.Mocker.SetMockSingletonInstance('icRepoSubscription', new icREPOSubscriptionMOck());

        icLogicSubscription.IClass BL = (icLogicSubscription.IClass) icObjectFactory.GetSingletonInstance('icLogicSubscription');


        BL.createGreenSkySubscription(new icDTOGreenSkySubscription());

    }

    public static testMethod void test_updateEagleViewSubscription(){
        initTest();

        icLogicSubscription.IClass BL = (icLogicSubscription.IClass) icObjectFactory.GetSingletonInstance('icLogicSubscription');

        IKOPRO_Contractor_Subscription__c s = new IKOPRO_Contractor_Subscription__c();
        s.Id = icTestHelperUtility.getFakeId(IKOPRO_Contractor_Subscription__c.SObjectType);

        BL.updateEagleViewSubscription(s);

    }

}