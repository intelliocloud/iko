public class Invoice_Line_TriggerHandler {

    public static void setInvoiceLineCurrency(List<Invoice_Line__c> allInvoiceLines) {
        
        // Get the rebate invoice IDs into a set so we can query stuff
        Set<Id> rebateInvoiceIds = new Set<Id>();
        for(Invoice_Line__c line : allInvoiceLines) {
            rebateInvoiceIds.add(line.Invoice__c);
        }
        
        List<Invoice__c> rebateInvoices = new List<Invoice__c>([SELECT Id, Contractor__c FROM Invoice__c WHERE Id = :rebateInvoiceIds]);
		// We can't do Contractor__r.CurrencyIsoCode in a Trigger, so we have to query again. Ugh.
		Map<Id, Invoice__c> invoicesById = new Map<Id, Invoice__c>();
        
        Set<Id> contractorIds = new Set<Id>();
        for(Invoice__c i : rebateInvoices) {
            contractorIds.add(i.Contractor__c);
            invoicesById.put(i.Id, i);
        }
        List<Account> allContractors = new List<Account>([SELECT Id, CurrencyIsoCode FROM Account WHERE Id IN :contractorIds]);
            Map<Id, Account> contractorsById = new Map<Id, Account>();
            for(Account a : allContractors) {
                contractorsById.put(a.Id, a);
            }

       // Now set the Currency which means we need to get the contractor ID from the rebate invoice
        for(Invoice_Line__c line : allInvoiceLines) {
            String invoiceID = line.Invoice__c;
            String contractorId = invoicesById.get(line.Invoice__c).Contractor__c;
            line.CurrencyIsoCode = contractorsById.get(contractorId).CurrencyIsoCode;
        }
        
    }
}