/**
 * Created by Francois Poirier on 2019-11-14.
 */

@isTest
public with sharing class icTestHandlerAccount {

    @isTest
    static void testAfterInsert(){

        test.setMock(HttpCalloutMock.class, new icHTTPCalloutServiceMock());
        icTestMockUtilities.Mocker.SetMockSingletonInstance('icLogicAccount', new icLogicAccountMock());
        icHandlerAccount.IClass handler = (icHandlerAccount.IClass) icObjectFactory.GetSingletonInstance('icHandlerAccount');


        List<Account> oldAccounts = new List<Account>();
        List<Account> newAccounts = new List<Account>();

        Account testAccount = new Account();
        testAccount.Admin_Email_Address__c = icTestHelperUtility.generateRandomString(10);
        testAccount.BillingStreet = icTestHelperUtility.generateRandomString(15);
        testAccount.DBA_Account__c = icTestHelperUtility.generateRandomString(10);
        testAccount.Website = icTestHelperUtility.generateRandomString(10);
        testAccount.Fax = icTestHelperUtility.generateRandomString(10);
        testAccount.Language_Preferences__c = icTestHelperUtility.generateRandomString(10);

        newAccounts.add(testAccount);

        icTestMockUtilities.Tracer.SetReturnValue('icLogicAccountMock', 'getAccountsForCSSync', newAccounts);

        handler.onAfterInsert(newAccounts, new Map<Id, Account>());


    }

    @isTest
    static void testAfterUpdate(){

        test.setMock(HttpCalloutMock.class, new icHTTPCalloutServiceMock());
        icTestMockUtilities.Mocker.SetMockSingletonInstance('icLogicAccount', new icLogicAccountMock());
        icHandlerAccount.IClass handler = (icHandlerAccount.IClass) icObjectFactory.GetSingletonInstance('icHandlerAccount');

        List<Account> oldAccounts = new List<Account>();
        List<Account> newAccounts = new List<Account>();

        Account testAccount = new Account();
        testAccount.Admin_Email_Address__c = icTestHelperUtility.generateRandomString(10);
        testAccount.BillingStreet = icTestHelperUtility.generateRandomString(15);
        testAccount.DBA_Account__c = icTestHelperUtility.generateRandomString(10);
        testAccount.Website = icTestHelperUtility.generateRandomString(10);
        testAccount.Fax = icTestHelperUtility.generateRandomString(10);
        testAccount.Language_Preferences__c = icTestHelperUtility.generateRandomString(10);
        newAccounts.add(testAccount);

        icTestMockUtilities.Tracer.SetReturnValue('icLogicAccountMock', 'getAccountsForCSSync', newAccounts);

        handler.onAfterUpdate(oldAccounts, newAccounts, new Map<Id, Account>());


    }

    @isTest
    static void testBeforeUpdate(){

        test.setMock(HttpCalloutMock.class, new icHTTPCalloutServiceMock());
        icTestMockUtilities.Mocker.SetMockSingletonInstance('icLogicAccount', new icLogicAccountMock());

        icHandlerAccount.IClass handler = (icHandlerAccount.IClass) icObjectFactory.GetSingletonInstance('icHandlerAccount');

        List<Account> oldAccounts = new List<Account>();
        List<Account> newAccounts = new List<Account>();

        Account testAccount = new Account();
        testAccount.Admin_Email_Address__c = icTestHelperUtility.generateRandomString(10);
        testAccount.BillingStreet = icTestHelperUtility.generateRandomString(15);
        testAccount.DBA_Account__c = icTestHelperUtility.generateRandomString(10);
        testAccount.Website = icTestHelperUtility.generateRandomString(10);
        testAccount.Fax = icTestHelperUtility.generateRandomString(10);
        testAccount.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('IKOPRO_Contractor').getRecordTypeId();
        testAccount.Language_Preferences__c = icTestHelperUtility.generateRandomString(10);
        newAccounts.add(testAccount);

        icTestMockUtilities.Tracer.SetReturnValue('icLogicAccountMock', 'getAccountsForCSSync', newAccounts);
        icTestMockUtilities.Tracer.SetReturnValue('icLogicAccountMock', 'hasKeyFieldsChangedForCSSync', TRUE);
        icTestMockUtilities.Tracer.SetReturnValue('icLogicAccountMock', 'hasAllRequiredFieldsForCSSync', TRUE);

        handler.onBeforeUpdate(oldAccounts, newAccounts, new Map<Id, Account>());


    }
}