/**
 * Created by Francois Poirier on 2020-01-21.
 */

global with sharing class icBatchUploadDraftInvoicesWithFiles implements Database.Batchable<sObject>{

    icLogicInvoice.IClass invoiceLogic = (icLogicInvoice.IClass) icObjectFactory.GetSingletonInstance('icLogicInvoice');

    global Database.QueryLocator start(Database.BatchableContext context) {

        return Database.getQueryLocator('SELECT Id, Status__c FROM Contractor_Invoice__c WHERE Status__c = \'Draft\' AND File_s_Attached__c = true');

    }

    global void execute(Database.BatchableContext context, List<Contractor_Invoice__c> scope) {

        for(Contractor_Invoice__c invoice : scope){
            invoice.Status__c = 'Ready for Upload';
        }

        invoiceLogic.updateInvoices(scope);

    }

    global void finish(Database.BatchableContext context) {
    }
}