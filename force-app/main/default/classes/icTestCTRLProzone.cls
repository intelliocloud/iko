/**
 * Created by Andrea Pissinis on 2018-09-13.
 */
@isTest
public with sharing class icTestCTRLProzone {

    public static testMethod void test_userCTRL(){
        icCTRLProzone.getCurrentTheme();
        icTestMockUtilities.Mocker.SetMockSingletonInstance('icLogicUser', new icLogicUserMock());
        icTestMockUtilities.Tracer.SetReturnValue('icLogicUserMock', 'getAccountIdFromPartnerUser', 'test');

        icCTRLProzone.getProzoneRemoteAuthDetails('');
    }

}