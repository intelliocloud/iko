/**
 * Created by Andrea Pissinis on 2018-09-13.
 */
@isTest
public with sharing class icTestCTRLEagleView {

    static void initTest(){
        icTestMockUtilities.Mocker.SetMockSingletonInstance('icLogicUser', new icLogicUserMock());
        icTestMockUtilities.Mocker.SetMockSingletonInstance('icLogicSubscription', new icLogicSubscriptionMock());
        icTestMockUtilities.Mocker.SetMockSingletonInstance('icBLeagleView', new icBLEagleViewMock());
        icTestMockUtilities.Mocker.SetMockSingletonInstance('icLogicEagleViewReport', new icLogicEagleViewReportMock());

        icTestMockUtilities.Tracer.SetReturnValue('icLogicUserMock', 'getAccountIdFromPartnerUser', icTestHelperUtility.getFakeId(Account.SObjectType));        
        icTestMockUtilities.Tracer.SetReturnValue('icBLEagleViewMock', 'getAvailableProducts', new List<icDTOEagleViewProduct>{new icDTOEagleViewProduct()});
    }

    public static testMethod void test_isCustomerSubscribed(){
        initTest();
        icTestMockUtilities.Tracer.SetReturnValue('icLogicSubscriptionMock', 'getEagleViewSubscriptionForCustomer', new List<IKOPRO_Contractor_Subscription__c>{new IKOPRO_Contractor_Subscription__c()});
        icCTRLEagleview.isCustomerSubscribed();
    }

    public static testMethod void test_isCommunity_true(){
        initTest();
        icCTRLEagleview.isCommunity('https://iko--l3.lightning.force.com');
    }

    public static testMethod void test_isCommunity_false(){
        initTest();
        icCTRLEagleview.isCommunity('https://iko--l3.my.force.com');
    }

    public static testMethod void test_getAvailableProducts(){
        initTest();
        icTestMockUtilities.Tracer.SetReturnValue('icLogicSubscriptionMock', 'getEagleViewSubscriptionForCustomer', new List<IKOPRO_Contractor_Subscription__c>{new IKOPRO_Contractor_Subscription__c()});
        icCTRLEagleview.getAvailableProducts();
    }

    public static testMethod void test_placeOrder(){
        initTest();
        icTestMockUtilities.Tracer.SetReturnValue('icLogicSubscriptionMock', 'getEagleViewSubscriptionForCustomer', new List<IKOPRO_Contractor_Subscription__c>{new IKOPRO_Contractor_Subscription__c()});
        icCTRLEagleview.placeOrder('');        
    }

    public static testMethod void test_placeOrder_exception(){
        initTest();
        List<IKOPRO_Contractor_Subscription__c> mockListNull;
        icTestMockUtilities.Tracer.SetReturnValue('icLogicSubscriptionMock', 'getEagleViewSubscriptionForCustomer', mockListNull);
        icCTRLEagleview.placeOrder('');
    }

    public static testMethod void test_createOrder(){
        initTest();
        icTestMockUtilities.Tracer.SetReturnValue('icLogicEagleViewReportMock', 'generateEagleViewReportFromOrderForm', new EagleView_Report__c());
        icCTRLEagleview.createOrder('{"order":"order"}', icTestHelperUtility.getFakeId(Opportunity.SObjectType));
    }

    public static testMethod void test_createOrder_exception(){
        initTest();
         EagleView_Report__c mockNull;
        icTestMockUtilities.Tracer.SetReturnValue('icLogicEagleViewReportMock', 'generateEagleViewReportFromOrderForm', mockNull);
        icCTRLEagleview.createOrder('{"order":"order"}', icTestHelperUtility.getFakeId(Opportunity.SObjectType));
    }

    public static testMethod void test_setOrderIds_withOrderId(){
        initTest();
        icDTOEagleViewPlaceOrderResponse mockDTO = new icDTOEagleViewPlaceOrderResponse();
        mockDTO.OrderId = 1;
        mockDTO.ReportIds = new List<Integer> {1};
        icCTRLEagleview.setOrderIds(icTestHelperUtility.getFakeId(EagleView_Report__c.SObjectType), JSON.serialize(mockDTO));
    }

    public static testMethod void test_setOrderIds_noOrderId(){
        initTest();
        icCTRLEagleview.setOrderIds(icTestHelperUtility.getFakeId(EagleView_Report__c.SObjectType), '{"order":"order"}');
    }

    public static testMethod void test_setOrderIds_Exception(){
        initTest();
        icCTRLEagleview.setOrderIds(icTestHelperUtility.getFakeId(EagleView_Report__c.SObjectType), null);
    }

    public static testMethod void test_cancelOrder(){
        initTest();
        icTestMockUtilities.Tracer.SetReturnValue('icLogicEagleViewReportMock', 'getEagleViewReportById', new EagleView_Report__c());
        icTestMockUtilities.Tracer.SetReturnValue('icLogicSubscriptionMock', 'getEagleViewSubscriptionForCustomer', new List<IKOPRO_Contractor_Subscription__c>{new IKOPRO_Contractor_Subscription__c()});
        icCTRLEagleview.cancelOrder('');
    }

    public static testMethod void test_cancelOrder_exception(){
        initTest();        
        icCTRLEagleview.cancelOrder('');
    }

    public static testMethod void test_subscribe_create(){
        initTest();
        icDTOGenericResult gr = new icDTOGenericResult();
        gr.isSuccess = true;
        gr.message = 'test';
        icTestMockUtilities.Tracer.SetReturnValue('icBLEagleViewMock', 'grantUserOrderDelegation', gr);
        icTestMockUtilities.Tracer.SetReturnValue('icLogicSubscriptionMock', 'getEagleViewSubscriptionForCustomer', new List<IKOPRO_Contractor_Subscription__c>{});
        icCTRLEagleview.subscribe('test','test');
    }

    public static testMethod void test_subscribe_update(){
        initTest();
        icDTOGenericResult gr = new icDTOGenericResult();
        gr.isSuccess = true;
        gr.message = 'test';
        icTestMockUtilities.Tracer.SetReturnValue('icBLEagleViewMock', 'grantUserOrderDelegation', gr);
        icTestMockUtilities.Tracer.SetReturnValue('icLogicSubscriptionMock', 'getEagleViewSubscriptionForCustomer', new List<IKOPRO_Contractor_Subscription__c>{new IKOPRO_Contractor_Subscription__c()});
        icCTRLEagleview.subscribe('test','test');
    }

    public static testMethod void test_subscribe_exception(){
        initTest();
        icDTOGenericResult gr;
        icTestMockUtilities.Tracer.SetReturnValue('icBLEagleViewMock', 'grantUserOrderDelegation', gr);
        icCTRLEagleview.subscribe('test','test');
    }
}