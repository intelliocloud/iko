/**
 * Created by Andrea Pissinis on 2018-09-13.
 */
@isTest
public with sharing class icLogicLeadMock implements icLogicLead.IClass{

    public Integer getNewLeadsCountByAccountId(String recordId, String ownerId) {
        Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'getNewLeadsCountByAccountId');
        params.put('recordId', recordId);
        params.put('ownerId', ownerId);
        return (Integer) icTestMockUtilities.Tracer.GetReturnValue(this, 'getNewLeadsCountByAccountId');
    }

    public Lead getLeadInfoById(Id leadId) {
        Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'getLeadInfoById');
        params.put('leadId', leadId);
        return (Lead) icTestMockUtilities.Tracer.GetReturnValue(this, 'getLeadInfoById');
    }

    public Boolean leadHasAddressPopulated(Lead l) {
        Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'leadHasAddressPopulated');
        params.put('l', l);
        return (Boolean) icTestMockUtilities.Tracer.GetReturnValue(this, 'leadHasAddressPopulated');
    }

    public Lead updateLead(Lead leadToUpdate) {
        Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'updateLead');
        params.put('leadToUpdate', leadToUpdate);
        return (Lead) icTestMockUtilities.Tracer.GetReturnValue(this, 'updateLead');
    }
}