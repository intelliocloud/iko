public with sharing class icServiceTierMovement implements icIClass {

	public Object GetInstance() {
		return new Impl();
	}

	public Interface IClass {
        icDtoTierMovementConfig getTierMovementConfig();
	}

	public class Impl implements IClass {

		public icDtoTierMovementConfig getTierMovementConfig() {
			icDtoTierMovementConfig returnConfig = new icDtoTierMovementConfig();

			returnConfig.runProcess = true;
			returnConfig.processMode = 'ascend';
			returnConfig.rebateYear = '2019';

			return returnConfig;
		}
	}
}