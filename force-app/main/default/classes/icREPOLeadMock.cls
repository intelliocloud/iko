/**
 * Created by Andrea Pissinis on 2018-09-12.
 */
@isTest
public with sharing class icREPOLeadMock implements icRepoLead.IClass{

    public List<Lead> getNewLeadsByAccountId(String recordId, String ownerId) {
        Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'getNewLeadsByAccountId');
        params.put('recordId', recordId);
        params.put('ownerId', ownerId);
        return (List<Lead>) icTestMockUtilities.Tracer.GetReturnValue(this, 'getNewLeadsByAccountId');
    }

    public Lead getLeadInfoById(Id leadId) {
        Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'getLeadInfoById');
        params.put('leadId', leadId);
        return (Lead) icTestMockUtilities.Tracer.GetReturnValue(this, 'getLeadInfoById');
    }

    public Lead updateLead(Lead leadToUpdate) {
        Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'updateLead');
        params.put('leadToUpdate', leadToUpdate);
        return (Lead) icTestMockUtilities.Tracer.GetReturnValue(this, 'updateLead');
    }
}