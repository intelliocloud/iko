public class icHandlerAccount implements icIClass {

	public Object GetInstance() {
		return new Impl();
	}

	public Interface IClass {
        void onBeforeUpdate(List<Account> oldAccounts, List<Account> newAccounts, Map<Id, Account> accountsMap);
		void onAfterInsert(List<Account> newAccounts, Map<Id, Account> accountsMap);
		void onAfterUpdate(List<Account> oldAccounts, List<Account> newAccounts, Map<Id, Account> accountsMap);
    }

	public class Impl implements IClass {

		icLogicAccount.IClass logicAccount = (icLogicAccount.IClass) icObjectFactory.GetSingletonInstance('icLogicAccount');

		public void onAfterInsert(List<Account> newAccounts, Map<Id, Account> accountsMap) {
			/*
            system.debug('OnAfterInsert');
            List<Account> accountsToSync = new List<Account>();
            List<Account> oldAccounts = new List<Account>();
            accountsToSync = logicAccount.getAccountsForCSSync(newAccounts, oldAccounts);
			system.debug(accountsToSync);
            if(accountsToSync.size() == 1) {
                icAsyncServiceCentralStation.syncAccount(accountsToSync[0].Id, serializeAccount(accountsToSync[0]));
            }
            */
		}

		public void onAfterUpdate(List<Account> oldAccounts, List<Account> newAccounts, Map<Id, Account> accountsMap) {
			/*
            system.debug('onAfterUpdate');
            List<Account> accountsToSync = new List<Account>();
            accountsToSync = logicAccount.getAccountsForCSSync(newAccounts, oldAccounts);
            system.debug(accountsToSync);
            if(accountsToSync.size() == 1) {
                icAsyncServiceCentralStation.syncAccount(accountsToSync[0].Id, serializeAccount(accountsToSync[0]));
            }
            */

            // loop trigger new; identify all account that...
            // if Mapics_ERP_ID__c get set (from null to VALUE)

            // for identified accounts, extract Mapics_ERP_ID__c  ==  List<String>
            // call logicAccount.getMapAccountsByMapicsExternalId == Map<String, Account>

            // [new method in logic/repo order] fetch orders where Customer_Number_Mapics__c IN extractedMapicsIds  == List<Orders>

            // loop in orders and set Orders.AccountId with accountmap.get(Customer_Number_Mapics__c).Id

		}

        public void onBeforeUpdate(List<Account> oldAccounts, List<Account> newAccounts, Map<Id, Account> accountsMap) {
            system.debug('onBEFOREUpdate');
            List<Account> accountsToSync = new List<Account>();
            Id contractorRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('IKOPRO_Contractor').getRecordTypeId();

            //accountsToSync = logicAccount.getAccountsForCSSync(newAccounts, oldAccounts);
            system.debug(accountsToSync);
            for(Account newAccount : newAccounts){
                Account oldAccount = accountsMap.get(newAccount.Id);

                if(newAccount.RecordTypeId == contractorRecordTypeId){
                    Boolean hasChanges = logicAccount.hasKeyFieldsChangedForCSSync(newAccount, oldAccount);
                    Boolean hasReq = logicAccount.hasAllRequiredFieldsForCSSync(newAccount);
                    //icServiceErrorHandler.StoreError('IKO-202 Troubleshoot', newAccount.Name, 'hasChanges : ' + hasChanges, 'hasReq : ' + hasReq, String.valueOf(Datetime.now()));

                    if(hasReq){
                        if(hasChanges) {
                            newAccount.CS_Sync_Required__c = TRUE;
                        }
                    } else {
                        newAccount.CS_Sync_Required__c = FALSE;
                    }
                }
            }
            //icServiceErrorHandler.CommitErrors();
        }
    }
}