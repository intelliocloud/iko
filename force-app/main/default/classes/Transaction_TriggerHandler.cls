public class Transaction_TriggerHandler {

    // SF-182 The Currency Code of the Transaction should match the Contractor
    // Contractor field is Account_ID__c is a master detail relationship, so it will always exist
    public static void setTransactionCurrency(List<Transaction__c> allTransactions) {
        
        //Create a list of unique Contractors so we can query
		set<Id> contractorIDs = new set<Id>();
        
        for(Transaction__c t : allTransactions) {
            contractorIDs.add(t.Account_ID__c);
        }
        
        // Get the currency iso codes and for teh contractors
        List<Account> allContractors = new List<Account>([SELECT Id, CurrencyIsoCode FROM Account WHERE Id IN :contractorIDs]);
        // Convert the list to a map
        Map<Id, Account> contractorsById = new Map<Id, Account>();
        for(Account a : allContractors) {
            contractorsById.put(a.Id, a);
        }
        
		// Set the Transaction currency field to match the Contractors by pulling it from the Map
		
        for(Transaction__c t : allTransactions) {
            t.CurrencyIsoCode = contractorsById.get(t.Account_ID__c).CurrencyIsoCode;
        }
        
    }
}