public class icRepoLead implements icIClass {

	public Object GetInstance() {
		return new Impl();
	}

	public Interface IClass {
		List<Lead> getNewLeadsByAccountId(String recordId, String ownerId);
        Lead getLeadInfoById(Id leadId);
		Lead updateLead(Lead leadToUpdate);
	}

	public class Impl implements IClass {

		public List<Lead> getNewLeadsByAccountId(String recordId, String ownerId) {
			String currentUserId = UserInfo.getUserId();
			List<Lead> listLeads = new List<Lead>();
			listLeads = [SELECT	Id
							,Name
					FROM	Lead
					WHERE	IsUnreadByOwner = true];
			//Referred_Customer__c = :recordId
			/*
							AND	(
								OwnerId = :currentUserId
								OR
								OwnerId = :ownerId
							)
			*/
			return listLeads;
		}
        
        public Lead getLeadInfoById(Id leadId) {

			List<Lead> listLead = [SELECT	Id,
											Name,
											PartnerAccountId,
											IsConverted,
											Rejected_Reason__c,
											Other_Rejected_Reason__c,
											Status,
											Address,
											Street,
											City,
											State,
											StateCode,
											PostalCode,
											CountryCode,
											OwnerId

									FROM	Lead
									WHERE	Id = :leadId
									LIMIT	1];
			if(listLead.size() == 1) {
				return listLead[0];
			}
			return null;
		}

		public Lead updateLead(Lead leadToUpdate) {
			update leadToUpdate;
			return leadToUpdate;
		}
	}
}