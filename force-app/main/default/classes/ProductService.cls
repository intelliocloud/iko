public  class ProductService {
   
    // this is to remove apostrophe before product code
    
    public static void removeApostrophe(List<Product2> records){

        for(Product2 productObj : records){

            if(String.isNotBlank(productObj.ProductCode)){
                productObj.ProductCode = productObj.ProductCode.removeStart('\'');
            }
        }
    }

}
