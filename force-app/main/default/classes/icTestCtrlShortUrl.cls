@isTest
public with sharing class icTestCtrlShortUrl {

	static void initTest() {
		icTestMockUtilities.Mocker.SetMockSingletonInstance('icLogicAccount', new icLogicAccountMock());
		icTestMockUtilities.Mocker.SetMockSingletonInstance('icLogicUser', new icLogicUserMock());
		icTestMockUtilities.Mocker.SetMockSingletonInstance('icServiceCentralStation', new icServiceCentralStationMock());

		icTestMockUtilities.Tracer.SetReturnValue('icLogicUserMock', 'getAccountIdFromPartnerUser', icTestHelperUtility.getFakeId(User.SObjectType));
		icTestMockUtilities.Tracer.SetReturnValue('icLogicAccountMock', 'getAcountInfoById', new Account(IKOPRO_QueueID__c = 'mockExtId', Central_Station_Short_URL__c = 'http://someurl.com'));
		icTestMockUtilities.Tracer.SetReturnValue('icServiceCentralStationMock', 'getShortUrl', 'http://someurl.com');		
	}

	public static testMethod void test_getShortUrlFromAccount() {
		initTest();
		String shortURLResult = icCtrlShortUrl.getShortUrlFromAccount();
		System.assertEquals(shortURLResult, 'http://someurl.com');
	}

	public static testMethod void test_getShortUrl() {
		initTest();
		String shortURLResult = icCtrlShortUrl.getShortUrl();
		System.assertEquals(shortURLResult, 'http://someurl.com');
	}
}