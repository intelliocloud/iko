public class Invoice_TriggerHandler {
    public static void setCurrencyIsoCode(List<Invoice__c> invoices) {
	
    // Build a Map with teh currency codes for all the contractors associated with this
    Set<Id> contractorIds = new Set<Id>();
        for(Invoice__c i : invoices) {
            contractorIds.add(i.Contractor__c);
        }
     
    List<Account> allContractors = new List<Account>([SELECT Id, CurrencyIsoCode FROM Account WHERE Id IN :contractorIds]);
    Map<Id, Account> contractorsById = new Map<Id, Account>();
        for(Account a : allContractors) {
            contractorsById.put(a.Id, a);
        }
        
        // Get the correct currency value from the Contractor map
        for(Invoice__c i : invoices){
            i.CurrencyIsoCode = contractorsById.get(i.Contractor__c).CurrencyIsoCode;
        }
    }
}