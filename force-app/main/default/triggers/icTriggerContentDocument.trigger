trigger icTriggerContentDocument on ContentDocument (before delete, before insert) {
	icHandlerContentDocument.IClass handler = (icHandlerContentDocument.IClass) icObjectFactory.GetSingletonInstance('icHandlerContentDocument');

	if(Trigger.isBefore && Trigger.isDelete) {
		handler.onBeforeDelete(Trigger.old, Trigger.oldMap);
	}
	System.debug('Trigger is calling');
	if(Trigger.isBefore && Trigger.isInsert) {
		handler.onBeforeInsert(trigger.new);
	}
}