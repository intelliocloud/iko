trigger icTriggerContentDocumentLink on ContentDocumentLink (after insert) {
	icHandlerContentDocument.IClass handler = (icHandlerContentDocument.IClass) icObjectFactory.GetSingletonInstance('icHandlerContentDocument');

	if(Trigger.isAfter && Trigger.isInsert) {
		System.debug('before trigger SOQL > ' + Limits.getQueries() + ' of ' + Limits.getLimitQueries());
		handler.onAfterInsert(Trigger.old, Trigger.new, Trigger.newMap);
		System.debug('after trigger SOQL > ' + Limits.getQueries() + ' of ' + Limits.getLimitQueries());
	}
}