trigger Invoice_Trigger on Invoice__c (before insert) {

    List<Invoice__c> allRebateInvoices = new List<Invoice__c>();
    allRebateInvoices.addAll(Trigger.new);
    
    // SF-177 Set the Rebate Invoice Currency Automatically
    // https://ikoussfsc.atlassian.net/browse/SF-185
    // Contractor is a master-detail relationship so this happens for all
    Invoice_TriggerHandler.setCurrencyIsoCode(allRebateInvoices);
}