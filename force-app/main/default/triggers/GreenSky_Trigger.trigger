trigger GreenSky_Trigger on GreenSky_Application__c (before insert) {

    // SF-184 Set Currency of Greensky Application Automatically
    // https://ikoussfsc.atlassian.net/browse/SF-184
    
    List<GreenSky_Application__c> allApplications = new List<GreenSky_Application__c>();
    allApplications.addAll(Trigger.new);
    
    // Contractor is the Account field. Technically it's not a master-detail, so we don't want to call this if it's empty
	List<GreenSky_Application__c> appsWithAccount = new List<GreenSky_Application__c>();
    for(GreenSky_Application__c g : allApplications) {
        if(g.Account__c != null) { appsWithAccount.add(g); }
        GreenSky_TriggerHandler.setCurrencyIsoCode(appsWithAccount);
    }
}