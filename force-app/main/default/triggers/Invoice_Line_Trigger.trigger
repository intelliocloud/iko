trigger Invoice_Line_Trigger on Invoice_Line__c (before insert) {

    // SF-183 Invoice Line should match the Currency of the Contractor
    // Contractor is actually a field on the Invoice so we need to go
    // Invoice_Line__c --> Invoice__c.Contractor__c
    List<Invoice_Line__c> allInvoiceLines = new List<Invoice_Line__c>();
    allInvoiceLines.addAll(Trigger.new);
    
    if(Trigger.isBefore == true && Trigger.isInsert == true) {
        Invoice_Line_TriggerHandler.setInvoiceLineCurrency(allInvoiceLines);
    }
}