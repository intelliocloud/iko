trigger Transaction_Trigger on Transaction__c (before insert, after insert) {

    list<Transaction__c> allTransactions = new list<Transaction__c>();
		allTransactions.addAll(Trigger.new);   
    
    // SF-182 The Currency Code of the Transaction should match the Contractor
    // Contractor field is Account_ID__c is a master detail relationship, so it will always exist
    if(Trigger.isBefore == true && Trigger.isInsert == true) {
        Transaction_TriggerHandler.setTransactionCurrency(allTransactions);        
    }
	    
}