/**
 * Created by Francois Poirier on 2019-06-11.
 */

trigger icCalendarEventTrigger on CalendarEvent__c (before insert, before update) {

    icLogicCalendarEventTrigger.IClass handler = (icLogicCalendarEventTrigger.IClass) icObjectFactory.GetSingletonInstance('icLogicCalendarEventTrigger');

    if(Trigger.isBefore && Trigger.isUpdate){

        handler.beforeUpdate(Trigger.old, Trigger.new, Trigger.oldMap, Trigger.newMap);

    }

    if(Trigger.isBefore && Trigger.isInsert){

        handler.beforeInsert(Trigger.new, Trigger.newMap);

    }
}